<?php

namespace AppBundle\Controller;

use AppBundle\Alibrary\AController;
use AppBundle\Alibrary\ChartFunctions;
use AppBundle\Entity\ChartHistory;
use Doctrine\Common\Util\Debug;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Chart;
use AppBundle\Form\ChartType;
use \SoapClient;

/**
 * Chart controller.
 *
 * @Route("/chart")
 */
class ChartController extends AController
{

    public $module = "Charts";

    /**
     * Lists all Chart entities.
     *
     * @Route("/", name="chart")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AppBundle:Chart')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Chart entity.
     *
     * @Route("/", name="chart_create")
     * @Method("POST")
     * @Template("AppBundle:Chart:new.html.twig")
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function createAction(Request $request)
    {
        $entity = new Chart();
        $form   = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em   = $this->getDoctrine()->getManager();
            $data = $form->getData();

            $maxSequenceId = $this->getSequenceId($data->getDashboardId());

            $entity->setData($data->getData());
            $entity->setSequenceId($maxSequenceId);
            $entity->setDashboard($data->getDashboardId());
            $entity->setDepartment($data->getdepartmentId());
            $entity->setType($data->getTypeId());

            $entity->preUpload();
            $em->persist($entity);
            $em->flush();
            $entity->upload();

            //update excel data
            if ($entity->getType()->getName() == "excel") {
                $objPHPExcel = $this->container->get('phpexcel')
                    ->createPHPExcelObject($entity->getAbsolutePath());
                $objWriter   = $this->container->get('phpexcel')
                    ->createWriter($objPHPExcel, 'HTML');
                ob_start();
                $objWriter->save('php://output');
                $entity->data = ob_get_clean();
                $em->flush();
            }

            $this->addFlash(
                'success',
                'Your changes were saved!'
            );

            return $this->redirect(
                $this->generateUrl(
                    'chart_show', array('id' => $entity->getId())
                )
            );
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Chart entity.
     *
     * @param Chart $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Chart $entity)
    {
        $form = $this->createForm(
            new ChartType(),
            $entity,
            array(
                'action' => $this->generateUrl('chart_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Chart entity.
     *
     * @Route("/new", name="chart_new")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function newAction()
    {
        $entity = new Chart();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Chart entity.
     *
     * @Route("/{id}", name="chart_show")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Chart')->find($id);

        if ( ! $entity) {
            throw $this->createNotFoundException(
                'Unable to find Chart entity.'
            );
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Chart entity.
     *
     * @Route("/{id}/edit", name="chart_edit")
     * @Method("GET")
     * @Template()
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AppBundle:Chart')->find($id);

        if ( ! $entity) {
            throw $this->createNotFoundException(
                'Unable to find Chart entity.'
            );
        }

        $editForm   = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Chart entity.
     *
     * @param Chart $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Chart $entity)
    {
        $form = $this->createForm(
            new ChartType(),
            $entity,
            array(
                'action' => $this->generateUrl(
                    'chart_update',
                    array('id' => $entity->getId())
                ),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Chart entity.
     *
     * @Route("/{id}", name="chart_update")
     * @Method("PUT")
     * @Template("AppBundle:Chart:edit.html.twig")
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function updateAction(Request $request, $id)
    {
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Chart')
            ->find($id);

        if ( ! $entity) {
            throw $this->createNotFoundException(
                'Unable to find Chart entity.'
            );
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createEditForm($entity);
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {

            $data = $editForm->getData();


            $maxSequenceId = $this->getSequenceId(
                $data->getDashboardId()->getId()
            );
            $entity->setSequenceId($maxSequenceId);

            $entity->setDashboardId($data->getDashboardId()->getId());
            $entity->setDepartmentId($data->getDepartmentId()->getId());

            $entity->setTypeId($data->getTypeId()->getId());

            $entity->preUpload();
            $em->flush();
            $entity->upload();

            //update excel data
            if ($entity->getType()->getName() == "excel") {
                $objPHPExcel = $this->container->get('phpexcel')
                    ->createPHPExcelObject($entity->getAbsolutePath());
                $objWriter   = $this->container->get('phpexcel')
                    ->createWriter($objPHPExcel, 'HTML');
                ob_start();
                $objWriter->save('php://output');
                $entity->data = ob_get_clean();
                $em->flush();
            }

            $this->addFlash(
                'success',
                'Your changes were saved!'
            );

            $this->addToHistory($entity);

            return $this->redirect(
                $this->generateUrl('chart_edit', array('id' => $id))
            );
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Chart entity.
     *
     * @Route("/{id}", name="chart_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_DELETE_CHARTS')")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em     = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Chart')->find($id);

            if ( ! $entity) {
                throw $this->createNotFoundException(
                    'Unable to find Chart entity.'
                );
            }

            $em->remove($entity);
            $em->flush();

            $this->addFlash(
                'success',
                'Chart Deleted!'
            );
        }

        return $this->redirect($this->generateUrl('chart'));
    }

    /**
     * Creates a form to delete a Chart entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('chart_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Deletes a Chart entity.
     *
     * @Route("_deleteFile", name="delete_file")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_MANAGE_CHARTS')")
     */
    public function deleteFileAction()
    {

        $request = $this->container->get('request');
        $id      = $request->query->get('id');

        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Chart')->find($id);

        if ( ! $entity) {
            throw $this->createNotFoundException(
                'Unable to find Chart entity.'
            );
        }
        $entity->removeUpload();
        $entity->file_path = "";
        $entity->data      = "";
        $em->flush();

        return new Response(json_encode(array("success" => true)));
    }

    /**
     * Deletes a Chart entity.
     *
     * @Route("_excel_show/{id}", name="excel_show")
     * @Method("GET")
     */
    public function excelShowAction($id)
    {

        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Chart')->find($id);

        if ( ! $entity) {
            throw $this->createNotFoundException(
                'Unable to find Chart entity.'
            );
        }

        echo $entity->data;
        exit;
    }

    /**
     * show a Chart entity from SQL.
     *
     * @Route("_sql_show/{id}", name="sql_show")
     * @Method("GET")
     * @Template("AppBundle:Chart:sql.html.twig")
     */
    public function sqlShowAction($id)
    {

        $params = array();
        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Chart')->find($id);

        if ( ! $entity) {
            throw $this->createNotFoundException(
                'Unable to find Chart entity.'
            );
        }

        $url = $entity->wsdlUrl . "/" . $entity->functionName . "?"
            . $entity->data;

        $data = file_get_contents($url);
        $xmlObject = simplexml_load_string($data);

        if ( ! empty($xmlObject[0])) {
            //get the actual data
            $xml = simplexml_load_string(
                $xmlObject[0], "SimpleXMLElement", LIBXML_NOCDATA
            );
            //convert to array
            $json  = json_encode($xml);
            $array = json_decode($json, true);

            //the first element is actually the main data
            $finalData = current($array);

            //get column data
            $columnArray = array_keys($finalData[0]);
//            echo "<pre>";print_r($columnArray);exit;

        }

        return [
            'data'        => $finalData,
            'columnArray' => $columnArray,
        ];
    }

    /**
     * show a Chart entity from SQL.
     *
     * @Route("_graph_show/{id}", name="show_graph")
     * @Method("GET")
     * @Template("AppBundle:Chart:graph.html.twig")
     */
    public function showGraphAction($id)
    {

        $em     = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppBundle:Chart')->find($id);

        $url = $entity->wsdlUrl . "/" . $entity->functionName . "?"
            . $entity->data;

        $data = file_get_contents($url);
        $xmlObject = simplexml_load_string($data);
        if ( ! empty($xmlObject[0])) {
            //get the actual data
            $xml = simplexml_load_string(
                $xmlObject[0], "SimpleXMLElement", LIBXML_NOCDATA
            );
            //convert to array
            $json  = json_encode($xml);
            $array = json_decode($json, true);

            //the first element is actually the main data
            $finalData = current($array);

            $chartLayout = 'AppBundle:Chart:Graph/'
                . $entity->layoutGraphScript . ".html.twig";

            $chartData = ChartFunctions::{
                $entity->layoutGraphScript}($finalData);
        }

        return [
            'chartLayout' => $chartLayout,
            'chartData'   => $chartData
        ];
    }


    protected function sendRawRequest($request, $url)
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl, CURLOPT_SSLVERSION, 1);

        $response = curl_exec($curl);

        if ($response === false || curl_errno($curl)) {
            error_log('IMPORTANT - CURL ERROR: ' . curl_error($curl));
        }
        curl_close($curl);

        return $response;
    }


    public function getSequenceId($dashboardId)
    {
        $em           = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQuery(
            'SELECT u.chartSequenceId FROM AppBundle:Chart u WHERE u.dashboardId = :id'
        )
            ->setMaxResults(1)
            ->setParameter('id', $dashboardId);

        return $queryBuilder->getOneOrNullResult()['chartSequenceId'] + 1;
    }

    public function addToHistory($entity)
    {
        $em            = $this->getDoctrine()->getManager();
        $historyEntity = new ChartHistory();
        $historyEntity->setChartId($entity->getId());
        $historyEntity->setName($entity->getName());
        $historyEntity->setData($entity->getData());
        $historyEntity->setSequenceId($entity->getSequenceId());
        $historyEntity->setDashboard($entity->getDashboard());
        $historyEntity->setDepartment($entity->getDepartment());
        $historyEntity->setType($entity->getType());
        $historyEntity->setWsdlUrl($entity->getWsdlUrl());
        $historyEntity->setFunctionName($entity->getFunctionName());
        $historyEntity->setTitle($entity->getTitle());
        $historyEntity->setDescription($entity->getDescription());
        $historyEntity->setFooter($entity->getFooter());
        $historyEntity->setOrderr($entity->getOrderr());
        $historyEntity->setShowTitle($entity->getShowTitle());
        $historyEntity->setWidth($entity->getWidth());
        $em->persist($historyEntity);
        $em->flush();
    }
}