<?php

namespace AppBundle\Controller;

use AppBundle\Alibrary\AController;
use AppBundle\Entity\GroupToDashboard;
use Doctrine\Common\Util\Debug;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Group;
use AppBundle\Form\GroupType;
use AppBundle\Entity\GroupToPermission;

/**
 * Group controller.
 *
 * @Route("/group")
 */
class GroupController extends AController {

	public $module = "Groups";

	/**
	 * Lists all Group entities.
	 *
	 * @Route("/", name="group")
	 * @Method("GET")
	 * @Security("has_role('ROLE_MANAGE_GROUPS')")
	 * @Template()
	 */
	public function indexAction() {
		$em = $this->getDoctrine()->getManager();

		$entities = $em->getRepository( 'AppBundle:Group' )->findAll();

		return array(
			'entities' => $entities,
		);
	}

	/**
	 * Creates a new Group entity.
	 *
	 * @Route("/", name="group_create")
	 * @Security("has_role('ROLE_MANAGE_GROUPS')")
	 * @Method("POST")
	 * @Template("AppBundle:Group:new.html.twig")
	 */
	public function createAction( Request $request ) {
		$entity = new Group();
		$form   = $this->createCreateForm( $entity );
		$form->handleRequest( $request );

		if ( $form->isValid() ) {
			$em = $this->getDoctrine()->getManager();
			$em->persist( $entity );
			$em->flush();

			$this->addFlash(
				'success',
				'Your changes were saved!'
			);

			return $this->redirect( $this->generateUrl( 'group_show', array( 'id' => $entity->getId() ) ) );
		}

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Creates a form to create a Group entity.
	 *
	 * @param Group $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm( Group $entity ) {
		$form = $this->createForm( new GroupType(), $entity, array(
			'action' => $this->generateUrl( 'group_create' ),
			'method' => 'POST',
		) );

		$form->add( 'submit', 'submit', array( 'label' => 'Create' ) );

		return $form;
	}

	/**
	 * Displays a form to create a new Group entity.
	 *
	 * @Route("/new", name="group_new")
	 * @Security("has_role('ROLE_MANAGE_GROUPS')")
	 * @Method("GET")
	 * @Template()
	 */
	public function newAction() {
		$entity = new Group();
		$form   = $this->createCreateForm( $entity );

		return array(
			'entity' => $entity,
			'form'   => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Group entity.
	 *
	 * @Route("/{id}", name="group_show")
	 * @Security("has_role('ROLE_MANAGE_GROUPS')")
	 * @Method("GET")
	 * @Template()
	 */
	public function showAction( $id ) {
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository( 'AppBundle:Group' )->find( $id );

		if ( ! $entity ) {
			throw $this->createNotFoundException( 'Unable to find Group entity.' );
		}

		$deleteForm = $this->createDeleteForm( $id );

		return array(
			'entity'      => $entity,
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Displays a form to edit an existing Group entity.
	 *
	 * @Route("/{id}/edit", name="group_edit")
	 * @Security("has_role('ROLE_MANAGE_GROUPS')")
	 * @Method("GET")
	 * @Template()
	 */
	public function editAction( $id ) {
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository( 'AppBundle:Group' )->find( $id );

		if ( ! $entity ) {
			throw $this->createNotFoundException( 'Unable to find Group entity.' );
		}

		$editForm   = $this->createEditForm( $entity );
		$deleteForm = $this->createDeleteForm( $id );

		return array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Creates a form to edit a Group entity.
	 *
	 * @param Group $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm( Group $entity ) {
		$form = $this->createForm( new GroupType(), $entity, array(
			'action' => $this->generateUrl( 'group_update', array( 'id' => $entity->getId() ) ),
			'method' => 'PUT',
		) );

		$form->add( 'submit', 'submit', array( 'label' => 'Update' ) );

		return $form;
	}

	/**
	 * Edits an existing Group entity.
	 *
	 * @Route("/{id}", name="group_update")
	 * @Security("has_role('ROLE_MANAGE_GROUPS')")
	 * @Method("PUT")
	 * @Template("AppBundle:Group:edit.html.twig")
	 */
	public function updateAction( Request $request, $id ) {
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository( 'AppBundle:Group' )->find( $id );

		if ( ! $entity ) {
			throw $this->createNotFoundException( 'Unable to find Group entity.' );
		}

		$deleteForm = $this->createDeleteForm( $id );
		$editForm   = $this->createEditForm( $entity );
		$editForm->handleRequest( $request );

		if ( $editForm->isValid() ) {
			$em->flush();

			$this->addFlash(
				'success',
				'Your changes were saved!'
			);

			return $this->redirect( $this->generateUrl( 'group_edit', array( 'id' => $id ) ) );
		}

		return array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Deletes a Group entity.
	 *
	 * @Route("/{id}", name="group_delete")
	 * @Security("has_role('ROLE_DELETE_GROUPS')")
	 * @Method("DELETE")
	 */
	public function deleteAction( Request $request, $id ) {
		$form = $this->createDeleteForm( $id );
		$form->handleRequest( $request );

		if ( $form->isValid() ) {
			$em     = $this->getDoctrine()->getManager();
			$entity = $em->getRepository( 'AppBundle:Group' )->find( $id );

			if ( ! $entity ) {
				throw $this->createNotFoundException( 'Unable to find Group entity.' );
			}

			$em->remove( $entity );
			$em->flush();

			$this->addFlash(
				'success',
				'Group deleted!'
			);
		}

		return $this->redirect( $this->generateUrl( 'group' ) );
	}

	/**
	 * Creates a form to delete a Group entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm( $id ) {
		return $this->createFormBuilder()
		            ->setAction( $this->generateUrl( 'group_delete', array( 'id' => $id ) ) )
		            ->setMethod( 'DELETE' )
		            ->add( 'submit', 'submit', array( 'label' => 'Delete' ) )
		            ->getForm();
	}

	/**
	 * Edits an existing Group entity.
	 *
	 * @Route("/{id}/permissions", name="permission_list")
	 * @Method("GET")
	 * @Template("AppBundle:Group:permissions.html.twig")
	 */
	public function permissionListAction( $id ) {
		$modules  = array();
		$values   = array();
		$em       = $this->getDoctrine()->getManager();
		$entities = $em->getRepository( 'AppBundle:Permission' )->findAll();

		//get current values
		$groupToPermissions = $em->getRepository( 'AppBundle:GroupToPermission' )
		                         ->findBy( array( "groupsId" => $id ) );
		foreach ( $groupToPermissions AS $val ) {
			$values[ $val->permission->getId() ] = TRUE;
		}

		foreach ( $entities AS $perm ) {
			$checked                                         = isset( $values[ $perm->getId() ] ) ? 'checked' : "";
			$modules[ $perm->getModule() ][ $perm->getId() ] = [
				'name'    => $perm->getName(),
				'checked' => $checked
			];
		}

		return array(
			'modules' => $modules,
			'id'      => $id,
		);
	}

	/**
	 * Edits an existing Group entity.
	 *
	 * @Route("/permission_update/{id}", name="group_permission_update")
	 * @Method("POST")
	 * @Template("AppBundle:Group:permissions.html.twig")
	 */
	public function updatePermissionAction( Request $request, $id ) {

		$em          = $this->getDoctrine()->getManager();
		$groupModel  = $em->getRepository( 'AppBundle:Group' )
		                  ->find( $id );
		$permissions = $request->get( 'permissions' );

		foreach ( $groupModel->groupToPermission AS $item ) {
			$em->remove( $item );
		}
		$em->flush();
		if ( ! empty( $permissions ) ) {
			foreach ( $permissions as $perm => $checked ) {
				$permId                = str_replace( "perm_", "", $perm );
				$permModel             = new GroupToPermission();
				$permModel->group      = $groupModel;
				$permModel->permission = $em->getRepository( 'AppBundle:Permission' )
				                            ->find( $permId );
				$em->persist( $permModel );
			}
			$em->flush();

			$this->addFlash(
				'success',
				'Permissions updated!'
			);
		}

		return $this->redirect( $this->generateUrl( 'permission_list', array( 'id' => $id ) ) );
	}


	/**
	 * Edits an existing Group entity.
	 *
	 * @Route("/{id}/dashboards", name="dashboard_list")
	 * @Method("GET")
	 * @Template("AppBundle:Group:dashboards.html.twig")
	 */
	public function dashboardListAction( $id ) {
		$modules  = array();
		$values   = array();
		$em       = $this->getDoctrine()->getManager();
		$entities = $em->getRepository( 'AppBundle:Dashboard' )->findAll();

		//get current values
		$groupToDashboard = $em->getRepository( 'AppBundle:GroupToDashboard' )
		                       ->findBy( array( "groupsId" => $id ) );
		foreach ( $groupToDashboard AS $val ) {
			$values[ $val->dashboard->getId() ] = TRUE;
		}

		foreach ( $entities AS $dash ) {
			$checked                                 = isset( $values[ $dash->getId() ] ) ? 'checked' : "";
			$modules["Dashboards"][ $dash->getId() ] = [
				'name'    => $dash->getName(),
				'checked' => $checked
			];
		}

		return array(
			'modules' => $modules,
			'id'      => $id,
		);
	}

	/**
	 * Edits an existing Group entity.
	 *
	 * @Route("/dashboard_update/{id}", name="group_dashboard_update")
	 * @Method("POST")
	 * @Template("AppBundle:Group:dashboards.html.twig")
	 */
	public function updateDashboardAction( Request $request, $id ) {

		$em         = $this->getDoctrine()->getManager();
		$groupModel = $em->getRepository( 'AppBundle:Group' )
		                 ->find( $id );
		$dashboards = $request->get( 'dashboards' );


		foreach ( $groupModel->groupToDashboard AS $item ) {
			$em->remove( $item );
		}
		$em->flush();
		if ( ! empty( $dashboards ) ) {
			foreach ( $dashboards as $dash => $checked ) {
				$dashId               = str_replace( "dash_", "", $dash );
				$dashModel            = new GroupToDashboard();
				$dashModel->group     = $groupModel;
				$dashModel->dashboard = $em->getRepository( 'AppBundle:Dashboard' )
				                           ->find( $dashId );
				$em->persist( $dashModel );
			}
			$em->flush();

			$this->addFlash(
				'success',
				'Dashboard updated!'
			);
		}

		return $this->redirect( $this->generateUrl( 'dashboard_list', array( 'id' => $id ) ) );
	}


}
