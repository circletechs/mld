<?php

namespace AppBundle\Controller;

use AppBundle\Alibrary\AController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Intl;

/**
 * @Route("/")
 *
 * @author Youssef Jradeh <youssef.jradeh@gmail.com>
 */
class HomeController extends AController {

  public $module = "Home";

  /**
   * @Route("/home", name="home_index")
   * @Template()
   */
  public function indexAction(Request $request) {
    $em = $this->getDoctrine()->getManager();
    $entities = $em->getRepository('AppBundle:Dashboard')->findAll();

    $current = $entities[0]->getId();

    return array(
      'current'           => $current,
      'entities'          => $entities,
      'charts'            => $this->getCharts($current),
      'allowedDashboards' => $this->getAccessDashboard(),
//      'baseurl'           => $request->getScheme() . '://'. $request->getHttpHost() . $request->getBasePath()
    );
  }

  /**
   * @Route("/home/dashboard/{id}", name="home_dashboard")
   * @Template("AppBundle:Home:index.html.twig")
   */
  public function dashboardAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();
    $entities = $em->getRepository('AppBundle:Dashboard')->findAll();

    return array(
      'current'           => $id,
      'entities'          => $entities,
      'charts'            => $this->getCharts($id),
      'allowedDashboards' => $this->getAccessDashboard(),
    );
  }

  private function getCharts($id) {

    if (!in_array($id, $this->getAccessDashboard())) {
      return [];
    }

    $em = $this->getDoctrine()->getManager();

    return $em->getRepository('AppBundle:Chart')
      ->findByDashboardId($id, array('order' => 'ASC'));
  }

  private function getAccessDashboard() {
    $return = [];
    $em = $this->getDoctrine()->getManager();
    $userCurrent = $this->get('security.token_storage')
      ->getToken()
      ->getUser();
    $accessDashboard = $em->getRepository('AppBundle:GroupToDashboard')
      ->findByGroupsId($userCurrent->groups->getId());

    foreach ($accessDashboard AS $dash) {
      $return[] = $dash->dashboard->getId();
    }

    return $return;
  }
}
