<?php

namespace AppBundle\Alibrary;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Chart controller.
 *
 * @Route("/chart")
 */
class AController extends Controller {

	public $module = "";

	public function accessCheck( $action = "Manage" ) {

		// keep in mind, this will call all registered security voters
		if ( FALSE === $this->get( 'security.authorization_checker' )
		                    ->isGranted( $action, $this->module )
		) {
			throw new AccessDeniedException( 'Unauthorised access!' );
		}
	}
}
