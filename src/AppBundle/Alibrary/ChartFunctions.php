<?php
/**
 * Created by PhpStorm.
 * User: yjradeh
 * Date: 3/4/16
 * Time: 4:07 PM
 */

namespace AppBundle\Alibrary;


class ChartFunctions
{

    public static function EservicesStats($data)
    {
        $titles     = [];
        $years      = [];
        $values     = [];
        $yearsOrder = [];

        foreach ($data AS $item) {
            if ( ! isset($titles[$item['Form_No']])) {
                $titles[$item['Form_No']] = $item['Form_Title'];
            }

            if ( ! isset($years[$item['Declarations_Year']])) {
                $years[$item['Declarations_Year']] = $item['Declarations_Year'];
            }

            $values[$item['Form_Title']][$item['Declarations_Year']]
                = $item['Declarations_Count'];
        }

//        echo "<pre>";
//        print_r($titles);
//        print_r($years);
//        print_r($values);
//        exit;

        foreach ($titles AS $title) {
            foreach ($years AS $year) {
                if (isset($values[$title][$year])) {
                    $yearsOrder[$year][] = $values[$title][$year];
                } else {
                    $yearsOrder[$year][] = 0;
                }
            }
        }

        return array(
            "titles" => $titles,
            "years"  => $yearsOrder
        );
    }
}