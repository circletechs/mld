<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Entity\Chart;

class ChartType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'name' , 'text' ,array(
				'label'       => 'Chart name (internal)',
			) )
			->add( 'title' , 'text' ,array(
				'label'       => 'Chart header (title)',
			) )
			->add( 'description' )
			->add( 'footer' , 'text' ,array(
				'label'       => 'Chart footer (reference, comments)',
			) )
			->add( 'typeId', 'entity', array(
				'class'       => 'AppBundle:ChartType',
				'property'    => 'name',
				'label'       => 'Type*',
				'placeholder' => 'Select one of the Type.',
				'required'    => TRUE,
				'data'        => $builder->getData()->getType()
			) )
			->add( 'file', NULL, array(
					'label' => 'Upload file (Image,Pdf or Excel) *',
				)
			)
			->add( 'wsdlUrl', 'url', array(
				'required' => FALSE
			) )
			->add( 'functionName', 'text', array(
				'required' => FALSE
			) )
			->add( 'layoutGraphScript', 'text', array(
				'required' => FALSE
			) )
			->add( 'departmentId', 'entity', array(
				'class'       => 'AppBundle:Department',
				'property'    => 'name',
				'label'       => 'Department*',
				'placeholder' => 'Select one of the Department.',
				'required'    => TRUE,
				'data'        => $builder->getData()->getDepartment()
			) )
			->add( 'data' )
			->add( 'dashboardId', 'entity', array(
				'class'       => 'AppBundle:Dashboard',
				'property'    => 'name',
				'label'       => 'Dashboard*',
				'placeholder' => 'Select one of the Dashboard.',
				'required'    => TRUE,
				'data'        => $builder->getData()->getDashboard()
			) )
			->add( 'showTitle' )
			->add( 'order' )
			->add( 'width', 'choice', array(
				'placeholder' => 'Select one of the below.',
				'choices'     => array(
					'0' => 'Half Width',
					'1' => 'Full Width'
				),
				'required'    => FALSE,
				'multiple'    => FALSE
			) );
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions( OptionsResolverInterface $resolver ) {
		$resolver->setDefaults( array(
			'data_class' => 'AppBundle\Entity\Chart',
			'required'   => TRUE,
		) );
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'appbundle_chart';
	}
}
