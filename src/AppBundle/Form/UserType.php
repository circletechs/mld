<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use AppBundle\Entity\User;


class UserType extends AbstractType {
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm( FormBuilderInterface $builder, array $options ) {
		$builder
			->add( 'username' )
			->add( 'email', 'email' )
			->add( 'firstname' )
			->add( 'lastname' )
			->add( 'enabled', NULL, array(
				'required' => FALSE
			) )
			->add( 'groupsId', 'entity', array(
				'class'       => 'AppBundle:Group',
				'property'    => 'name',
				'label'       => 'Group*',
				'placeholder' => 'Select one of the Groups.',
				'required'    => TRUE,
				'data'        => $builder->getData()->groups
			) );
		if ( ! $builder->getData()->getId() ) {
			$builder->add( 'password', 'password', array( 'required' => TRUE ) );
		}
	}

	/**
	 * @param OptionsResolverInterface $resolver
	 */
	public function setDefaultOptions( OptionsResolverInterface $resolver ) {
		$resolver->setDefaults( array(
			'data_class' => 'AppBundle\Entity\User',
			'required'   => TRUE,
		) );
	}

	/**
	 * @return string
	 */
	public function getName() {
		return 'appbundle_user';
	}
}
