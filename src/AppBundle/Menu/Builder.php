<?php
// src/AppBundle/Menu/Builder.php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware {
	public function mainMenu( FactoryInterface $factory, array $options ) {
		$menu = $factory->createItem( 'root' );

		$menu->addChild( 'Home', array( 'route' => 'home_index' ) );
		$menu->addChild( 'Charts', array( 'route' => 'chart' ) );
		$menu->addChild( 'Dashboards', array( 'route' => 'dashboard' ) );
		$menu->addChild( 'Departments', array( 'route' => 'department' ) );
		$menu->addChild( 'Users', array( 'route' => 'user' ) );
		$menu->addChild( 'Groups', array( 'route' => 'group' ) );
		if ( $this->container->get( 'security.authorization_checker' )
		                     ->isGranted( 'ROLE_SUPER_ADMIN' )
		) {
			$menu->addChild( 'Permissions', array( 'route' => 'permission' ) );
		}
		$menu->addChild( 'Logout', array( 'route' => 'fos_user_security_logout' ) );

		return $menu;
	}
}