<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="permission")
 * @ORM\Entity
 */
class Permission {
	public function __construct() {
		$this->groupToPermission = new ArrayCollection();
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", precision=0, scale=0,
	 *                        nullable=false, unique=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, precision=0,
	 *                          scale=0, nullable=false, unique=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="module", type="string", length=255, precision=0,
	 *                            scale=0, nullable=false, unique=false)
	 */
	private $module;


	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupToPermission", mappedBy="permission")
	 */
	public $groupToPermission;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Permission
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set module
	 *
	 * @param string $module
	 *
	 * @return Permission
	 */
	public function setModule( $module ) {
		$this->module = $module;

		return $this;
	}

	/**
	 * Get module
	 *
	 * @return string
	 */
	public function getModule() {
		return $this->module;
	}
}
