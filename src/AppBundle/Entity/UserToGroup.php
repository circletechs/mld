<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="user_to_group")
 * @ORM\Entity
 */
class UserToGroup {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", precision=0, scale=0,
	 *                        nullable=false, unique=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	public $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="groups_id", type="integer", nullable=false)
	 */
	public $groupsId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="user_id", type="integer", nullable=false)
	 */
	public $userId;


	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="userToGroup")
	 * @ORM\JoinColumn(name="groups_id", referencedColumnName="id")
	 */
	public $group;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="userToGroup")
	 * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
	 */
	public $user;
}
