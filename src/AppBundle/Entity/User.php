<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser {

	public function __construct() {
		$this->userToGroup = new ArrayCollection();
		parent::__construct();
	}

	/**
	 * @ORM\Id
	 * @ORM\Column(name="user_id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @Assert\NotBlank()
	 */
	protected $username;

	/**
	 * @Assert\Email()
	 * @Assert\NotBlank()
	 */
	protected $email;

	/**
	 * @Assert\NotBlank()
	 */
	protected $password;

	/**
	 * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 */
	private $firstname;

	/**
	 * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
	 * @Assert\NotBlank()
	 */
	private $lastname;

	/**
	 * @Assert\NotBlank()
	 *
	 * @ORM\Column(name="groups_id", type="integer", nullable=false)
	 */
	public $groupsId;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="users")
	 * @ORM\JoinColumn(name="groups_id", referencedColumnName="id")
	 */
	public $groups;

	/**
	 * Set firstname
	 *
	 * @param string $firstname
	 *
	 * @return User
	 */
	public function setFirstname( $firstname ) {
		$this->firstname = $firstname;

		return $this;
	}

	/**
	 * Get firstname
	 *
	 * @return string
	 */
	public function getFirstname() {
		return $this->firstname;
	}

	/**
	 * Set lastname
	 *
	 * @param string $lastname
	 *
	 * @return User
	 */
	public function setLastname( $lastname ) {
		$this->lastname = $lastname;

		return $this;
	}

	/**
	 * Get lastname
	 *
	 * @return string
	 */
	public function getLastname() {
		return $this->lastname;
	}


	/**
	 * Gets the groups granted to the user.
	 *
	 * @return Collection
	 */
	public function getGroups()
	{
		return $this->groups ?[$this->groups]: $this->groups = new ArrayCollection();
	}

}
