<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Group
 *
 * @ORM\Table(name="chart")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Chart
{
//    public  $types = array(
//        '1' => 'line-basic', //http://www.highcharts.com/demo/line-basic
//        '2' => 'pie', //http://www.highcharts.com/demo/3d-pie
//        '3' => 'column-stacked', //http://www.highcharts.com/demo/column-stacked
//        '4' => 'bar-stacked', //http://www.highcharts.com/demo/bar-stacked
//        '5' => 'combo-dual-axes', //http://www.highcharts.com/demo/combo-dual-axes
//        '6' => 'column-basic', //http://www.highcharts.com/demo/column-basic
//    );

    /**
     * @var integer
     *
     * @ORM\Column(name="chart_id", type="integer", precision=0, scale=0,
     *                              nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var datetime $created_at
     *
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @var datetime $updated_at
     *
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated_at;

    /**
     * @var integer
     *
     * @ORM\Column(name="chart_sequence_id", type="integer", nullable=false)
     */
    private $chartSequenceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="dashboard_id", type="integer", nullable=false)
     */
    private $dashboardId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0,
     *                          scale=0, nullable=false, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",precision=0, scale=0,
     *                                 nullable=true, unique=false)
     */
    public $description;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string",length=255, precision=0, scale=0,
     *                                 nullable=true, unique=false)
     */
    public $title;

    /**
     * @var string
     *
     * @ORM\Column(name="footer", type="text", precision=0, scale=0,
     *                                 nullable=true, unique=false)
     */
    public $footer;

    /**
     * @var integer
     *
     * @ORM\Column(name="type_id", type="integer", nullable=false)
     */
    public $typeId;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255, precision=0,
     *                          scale=0, nullable=true, unique=false)
     */
    public $file_path;

    /**
     * @var string
     *
     * @ORM\Column(name="data", type="text", precision=0, scale=0,
     *                                 nullable=true, unique=false)
     */
    public $data;

    /**
     * @var string
     *
     * @ORM\Column(name="wsdl_url", type="string", length=255, precision=0,
     *                          scale=0, nullable=false, unique=false)
     */
    public $wsdlUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="function_name", type="string", length=255, precision=0,
     *                          scale=0, nullable=false, unique=false)
     */
    public $functionName;

    /**
     * @var string
     *
     * @ORM\Column(name="layout_graph_script", type="string", length=255,
     *                                         precision=0, scale=0,
     *                                         nullable=true, unique=false)
     */
    public $layoutGraphScript;

    /**
     * @var integer
     *
     * @ORM\Column(name="department_id", type="integer", nullable=false)
     */
    private $departmentId;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ChartType", inversedBy="charts")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Department", inversedBy="charts")
     * @ORM\JoinColumn(name="department_id", referencedColumnName="department_id")
     */
    private $department;


    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dashboard", inversedBy="charts")
     * @ORM\JoinColumn(name="dashboard_id", referencedColumnName="dashboard_id")
     */
    private $dashboard;

    /**
     * @var integer
     *
     * @ORM\Column(name="orderr", type="integer", nullable=true)
     */
    public $order;

    /**
     * @var integer
     *
     * @ORM\Column(name="show_title", type="boolean", nullable=true)
     */
    public $showTitle;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     */
    public $width;

    /**
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    private $temp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param id $id
     *
     * @return Group
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets triggered only on insert
     *
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created_at = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update
     *
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated_at = new \DateTime("now");
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Chart
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Chart
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set footer
     *
     * @param string $footer
     *
     * @return Chart
     */
    public function setFooter($footer)
    {
        $this->footer = $footer;

        return $this;
    }

    /**
     * Get footer
     *
     * @return string
     */
    public function getFooter()
    {
        return $this->footer;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Chart
     */
    public function setTypeId($type)
    {
        $this->typeId = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set departmentId
     *
     * @param integer $departmentId
     *
     * @return Chart
     */
    public function setDepartmentId($departmentId)
    {
        $this->departmentId = $departmentId;

        return $this;
    }

    /**
     * Get departmentId
     *
     * @return integer
     */
    public function getDepartmentId()
    {
        return $this->departmentId;
    }

    /**
     * Set dashboardId
     *
     * @param integer $dashboardId
     *
     * @return Chart
     */
    public function setDashboardId($dashboardId)
    {
        $this->dashboardId = $dashboardId;

        return $this;
    }

    /**
     * Get dashboardId
     *
     * @return integer
     */
    public function getDashboardId()
    {
        return $this->dashboardId;
    }

    /**
     * Set department
     *
     * @param \AppBundle\Entity\Department $department
     *
     * @return Chart
     */
    public function setDepartment(\AppBundle\Entity\Department $department = null
    ) {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \AppBundle\Entity\Department
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set dashboard
     *
     * @param \AppBundle\Entity\Dashboard $dashboard
     *
     * @return Chart
     */
    public function setDashboard(\AppBundle\Entity\Dashboard $dashboard = null)
    {
        $this->dashboard = $dashboard;

        return $this;
    }

    /**
     * Get dashboard
     *
     * @return \AppBundle\Entity\Dashboard
     */
    public function getDashboard()
    {
        return $this->dashboard;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\ChartType $type
     *
     * @return Chart
     */
    public function setType(\AppBundle\Entity\ChartType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\ChartType
     */
    public function getType()
    {
        return $this->type;
    }


    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->file_path;
    }

    /**
     * @param string $file_path
     */
    public function setFilePath($file_path)
    {
        $this->file_path = $file_path;
    }

    /**
     * @return string
     */
    public function getWsdlUrl()
    {
        return $this->wsdlUrl;
    }

    /**
     * @param string $wsdlUrl
     */
    public function setWsdlUrl($wsdlUrl)
    {
        $this->wsdlUrl = $wsdlUrl;
    }

    /**
     * @return string
     */
    public function getFunctionName()
    {
        return $this->functionName;
    }

    /**
     * @param string $functionName
     */
    public function setFunctionName($functionName)
    {
        $this->functionName = $functionName;
    }

    /**
     * @return string
     */
    public function getOrderr()
    {
        return $this->title;
    }

    /**
     * @param string $orderr
     */
    public function setOrderr($orderr)
    {
        $this->orderr = $orderr;
    }

    /**
     * @return string
     */
    public function getShowTitle()
    {
        return $this->showTitle;
    }

    /**
     * @param string $showTitle
     */
    public function setShowTitle($showTitle)
    {
        $this->showTitle = $showTitle;
    }

    /**
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param string $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     *
     * @return Chart
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param mixed $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * Get sequence id
     *
     * @return integer
     */
    public function getSequenceId()
    {
        return $this->chartSequenceId;
    }

    /**
     * Set sequence id
     *
     * @param integer $sequenceId
     *
     * @return integer
     */
    public function setSequenceId($sequenceId)
    {
        $this->chartSequenceId = $sequenceId;

        return $this;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->file_path)) {
            // store the old name to delete after the update
            $this->temp      = $this->file_path;
            $this->file_path = null;
        } else {
            $this->file_path = 'initial';
        }
    }

    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename        = sha1(uniqid(mt_rand(), true));
            $this->file_path = $filename . '.' . $this->getFile()
                    ->guessExtension();

        }
    }

    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->file_path);

        // check if we have an old image
        if (isset($this->temp) && ! empty($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = null;
        }


        $this->file = null;
    }

    public function addToHistory()
    {
        $history = new ChartHistory();
        $history->setSequenceId($this->getSequenceId());
        $history->setDashboardId($this->getDashboardId());
    }

    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->file_path ? null
            : $this->getUploadRootDir() . '/' . $this->file_path;
    }

    public function getWebPath()
    {
        return null === $this->file_path ? null
            : "/" . $this->getUploadDir() . '/' . $this->file_path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/documents';
    }

    public function render()
    {
        $s = "";
        switch ($this->getType()->getName()) {
            case "image":
                $s = $this->renderImage();
                break;
            case "excel":
                $s = $this->renderExcel();
                break;
            case "sql":
                $s = $this->renderSql();
                break;
        }

        return $s;
    }

    public function renderImage()
    {
        if ( ! $this->file_path) {
            return "";
        }
        $s = "<div class='chart_title'><h3>" . ucwords($this->getTitle()) . "<span class='pull-right chart_last_updated text-danger' style='font-size:medium;
'> ( " . $this->checkDate() . " ) </span></h3></div>";
        $s .= "<div class='" . $this->getClassWidth() . "'>";
        if ($this->showTitle) {
            $s .= "<div class='chart_title'>" . $this->getName() . "</div>";
        }

        $s .= "<figure class='figure'>";
        $s .= "<a href='{$this->getWebPath()}' class='bootpopup' title='{$this->getName()}'><i class='fa fa-search-plus'></i>";
        $s .= "<img width='80%' class='chart_img figure-img img-fluid img-rounded img-responsive'  src='"
            . $this->getWebPath() . "'></img>";
        $s .= "</a>";
        $s .= "<figcaption class='figure-caption'>" . $this->getFooter()
            . "</figcaption>";
        $s .= "</figure>";
        $s .= "</div>";
        $s .= "<div class='" . $this->getSecondClassWidth() . "'>";
        if ($this->getDescription()) {
            $s .= "<div class='chart_description'><b>Details :</b>"
                . $this->getDescription() . "</div>";
        }
        $s .= "</div>";


        return $s;
    }

    public function renderExcel()
    {
        if ( ! $this->file_path) {
            return "";
        }
        $s = "<div class='chart_title'><h3>" . ucwords($this->getTitle()) . "<span class='pull-right chart_last_updated text-danger' style='font-size:medium;
'> ( " . $this->checkDate() . " ) </span></h3></div>";
        $s .= "<div class='" . $this->getClassWidth() . "'>";
        if ($this->showTitle) {
            $s .= "<div class='chart_title'>" . $this->getName() . "  </div>";
        }

        $s .= "<div class='chart_frame'><iframe width='80%'  frameBorder='0' src='/chart_excel_show/"
            . $this->id . "'></iframe>
		<a cl target='_blank' href='/chart_excel_show/" . $this->id
            . "'><i class='fa fa-search-plus'></i></a></div>";
        $s .= "</div>";
        $s .= "<div class='" . $this->getSecondClassWidth() . "'>";
        if ($this->getDescription()) {
            $s .= "<div class='chart_description'><b>Details :</b>"
                . $this->getDescription() . "</div>";
        }

        $s .= "</div>";

        return $s;
    }

    public function renderSql()
    {
        if ( ! $this->wsdlUrl || ! $this->functionName) {
            return "";
        }

        $s = "<div class='chart_title'><h3>" . ucwords($this->getTitle()) . "<span class='pull-right chart_last_updated text-danger' style='font-size:medium;
'> ( " . $this->checkDate() . " ) </span></h3></div>";
        $s .= "<div class='" . $this->getClassWidth() . "'>";
        if ($this->showTitle) {
            $s .= "<div class='chart_title'>" . $this->getName() . "</div>";
        }

        $s .= "<div class='chart_frame'><iframe width='80%' frameBorder='0' src='/chart_sql_show/"
            . $this->id . "'></iframe></div>";

        $s .= "</div>";
        $s .= "<div class='" . $this->getSecondClassWidth() . "'>";
        if ($this->getDescription()) {
            $s .= "<div class='chart_description'><b>Details :</b>"
                . $this->getDescription() . "</div>";
        }

        $s .= "</div>";


        if ($this->layoutGraphScript) {
            $s .= "<div class='col-sm-12 chart_frame'>
                    <iframe width='100%' frameBorder='0' src='/chart_graph_show/" . $this->id . "' scrolling='no'  onload='this.style.height = this.contentWindow.document.body.scrollHeight + \"px\";' style='border:0 !important' />
                    </iframe>
                    </div>";

        }

        return $s;
    }

    private function checkDate()
    {
        if ( ! $this->updated_at) {
            return "<b>New : </b>" . $this->created_at->format('d/m/Y - H:i');
        }

        return "<b>Last Updated : </b>" . $this->updated_at->format(
            'd/m/Y - H:i'
        );
    }

    private function getClassWidth()
    {
        if ( ! $this->width) {
            return "col-lg-6 col-sm-12";
        }

        return "col-lg-9 col-sm-12";
    }

    private function getSecondClassWidth()
    {
        if ( ! $this->width) {
            return "col-lg-6 col-sm-12";
        }

        return "col-lg-3 col-sm-12";
    }
}
