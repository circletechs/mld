<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dashboard
 *
 * @ORM\Table(name="group_to_dashboard")
 * @ORM\Entity
 */
class GroupToDashboard {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", precision=0, scale=0,
	 *                        nullable=false, unique=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	public $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="groups_id", type="integer", nullable=false)
	 */
	public $groupsId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="dashboard_id", type="integer", nullable=false)
	 */
	public $dashboardId;


	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="groupToDashboard")
	 * @ORM\JoinColumn(name="groups_id", referencedColumnName="id")
	 */
	public $group;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dashboard", inversedBy="groupToDashboard")
	 * @ORM\JoinColumn(name="dashboard_id", referencedColumnName="dashboard_id")
	 */
	public $dashboard;
}
