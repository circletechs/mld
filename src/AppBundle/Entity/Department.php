<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Table(name="department")
 * @ORM\Entity
 */
class Department
{
    public function __construct()
    {
        $this->charts = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="department_id", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, precision=0, scale=0, nullable=false, unique=false)
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Chart", mappedBy="department")
     */
    public $charts;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add charts
     *
     * @param \AppBundle\Entity\Chart $charts
     * @return Department
     */
    public function addChart(\AppBundle\Entity\Chart $charts)
    {
        $this->charts[] = $charts;

        return $this;
    }

    /**
     * Remove charts
     *
     * @param \AppBundle\Entity\Chart $charts
     */
    public function removeChart(\AppBundle\Entity\Chart $charts)
    {
        $this->charts->removeElement($charts);
    }

    /**
     * Get charts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCharts()
    {
        return $this->charts;
    }
}
