<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Permission
 *
 * @ORM\Table(name="group_to_permission")
 * @ORM\Entity
 */
class GroupToPermission {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", precision=0, scale=0,
	 *                        nullable=false, unique=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	public $id;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="groups_id", type="integer", nullable=false)
	 */
	public $groupsId;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="permission_id", type="integer", nullable=false)
	 */
	public $permissionId;


	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group", inversedBy="groupToPermission")
	 * @ORM\JoinColumn(name="groups_id", referencedColumnName="id")
	 */
	public $group;

	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Permission", inversedBy="groupToPermission")
	 * @ORM\JoinColumn(name="permission_id", referencedColumnName="id")
	 */
	public $permission;
}
