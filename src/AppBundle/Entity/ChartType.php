<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ChartType
 *
 * @ORM\Table(name="chart_type")
 * @ORM\Entity
 */
class ChartType {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", precision=0, scale=0,
	 *                        nullable=false, unique=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, precision=0,
	 *                          scale=0, nullable=false, unique=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="type", type="string", length=50, precision=0,
	 *                            scale=0, nullable=false, unique=false)
	 */
	private $type;


	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Chart", mappedBy="type")
	 */
	public $charts;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Permission
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 *
	 * @return ChartType
	 */
	public function setType( $type ) {
		$this->type = $type;

		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
}
