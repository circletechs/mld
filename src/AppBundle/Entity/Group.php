<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\GroupInterface;

/**
 * Group
 *
 * @ORM\Table(name="groups")
 * @ORM\Entity
 */
class Group implements GroupInterface {
	public function __construct() {
		$this->groupToPermission = new ArrayCollection();
	}

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", precision=0, scale=0,
	 *                        nullable=false, unique=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, precision=0,
	 *                          scale=0, nullable=false, unique=false)
	 */
	private $name;


	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupToPermission", mappedBy="group")
	 */
	public $groupToPermission;

	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\GroupToDashboard", mappedBy="group")
	 */
	public $groupToDashboard;

	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\User", mappedBy="groups")
	 */
	public $users;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 *
	 * @return Group
	 */
	public function setName( $name ) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * @param string $role
	 *
	 * @return self
	 */
	public function addRole( $role ) {
		// TODO: Implement addRole() method.
	}

	/**
	 * @param string $role
	 *
	 * @return boolean
	 */
	public function hasRole( $role ) {
		// TODO: Implement hasRole() method.
	}

	/**
	 * @return array
	 */
	public function getRoles() {
		$toReturn = [];

		if ( $this->groupToPermission ) {
			foreach ( $this->groupToPermission AS $perm ) {
				$toReturn[] = $perm->permission->getName();
			}
		}
		return $toReturn;
	}

	/**
	 * @param string $role
	 *
	 * @return self
	 */
	public function removeRole( $role ) {
		// TODO: Implement removeRole() method.
	}

	/**
	 * @param array $roles
	 *
	 * @return self
	 */
	public function setRoles( array $roles ) {
		// TODO: Implement setRoles() method.
	}
}
